let users = [
    {
        "sn": 1,
        "name": "Aditya Solanki",
        "contact": "3355978186"
    }, {
        "sn": 2,
        "name": "Ved Thakur",
        "contact": "6841483001"
    }, {
        "sn": 3,
        "name": "Rohan Singh",
        "contact": "5467467784"
    }, {
        "sn": 4,
        "name": "Rohit Raghuram",
        "contact": "9657598785"
    }, {
        "sn": 5,
        "name": "Pranav Mohril",
        "contact": "7856098789"
    }
]
let sn =  users.length + 1;
for (let user of users) {
    let table = $("#table-body");
    let row = `<tr>
                    <td class ="srno">${user.sn}</td>
                    <td class ="name">${user.name}</td>
                    <td class ="contact">${user.contact}</td>
                    <td class = "delete">x</td>
                </tr>`;
    table.append(row);
}

// Create User
function createUser() {
    let name = $("#firstName").val() + " " + $("#lastName").val();
    let contact = $("#phoneNumber").val();

    let newUser = {
        "sn": sn,
        "name": name,
        "contact": contact
    }

    users.push(newUser);

    // Update Table
    let table = $("#table-body");

    let row = `<tr>
                    <td class ="srno">${sn}</td>
                    <td class ="name">${name}</td>
                    <td class ="contact">${contact}</td>
                    <td class = "delete">x</td>
                </tr>`;
    table.append(row);

    sn++;
    console.log(users);

}

// Validate UserName and Contact
function validateName() {
    let name = $("#firstName").val() + " " + $("#lastName").val();

    for (let user of users) {
        if (user.name === name) {
            return false;
        }
    }
    return true;
}

function validateContact() {
    let contact = $("#phoneNumber").val();

    if (contact.length !== 10) 
        return false;
    for (let user of users) {
        if (user.contact === contact) {
            return false;
        }
    }
    return true;
}

// Clear Form
function clearForm() {
    $("#firstName").val("");
    $("#lastName").val("");
    $("#phoneNumber").val("");
}

// Submit Form
$("#user-form").submit(function (e) {
    e.preventDefault();
    if (!validateName()) {
        alert("Name Already Exists");
        clearForm();
        return;
    }
    if (!validateContact()) {
        alert("Contact Already Exists or Invalid");
        clearForm();
        return;
    }
    createUser();
    clearForm();
});

function deleteUser(userDel) {
    let index = users.findIndex(user => user.name === userDel);
    users.splice(index, 1);
    console.log(users);
}

// Delete User Entry
$(document).on('click', '.delete', function (e) {
    e.preventDefault();
    console.log("clicked");
    let alert = "Are you sure you want to delete this Entry?"

    if (confirm(alert)) {
        $(this)
            .parent()
            .remove();
        let userDel = $(this)
            .siblings(".name")
            .text();
        deleteUser(userDel);
        console.log("Entry Deleted");
    } else {
        console.log("Entry Not Deleted");
    }
});

// Search User
$("#search").keyup(function () {
    let search = $("#search")
        .val()
        .toLowerCase();
    let table = $("#table-body tr");

    table.filter(function () {
        $(this).toggle($(this).text().toLowerCase().indexOf(search) > -1);
    });
});

let flag = true;
// Sort User by Name
$(document).on('click', '#name-sort', function () {
    let table = $("#table-body tr");
    let sorted
    if (flag) {
        sorted = table.sort(function (a, b) {
            let aName = $(a)
                .find(".name")
                .text();
            let bName = $(b)
                .find(".name")
                .text();
            return aName.localeCompare(bName);
        });
        flag = false;
    } else {
        sorted = table.sort(function (a, b) {
            let aName = $(a)
                .find(".name")
                .text();
            let bName = $(b)
                .find(".name")
                .text();
            return bName.localeCompare(aName);
        });
        flag = true;
    }
    $("#table-body").html(sorted);
});